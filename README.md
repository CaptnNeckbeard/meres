# README #

### Requirements ###

* Python 3.7 
* opencv-python (https://pypi.org/project/opencv-python/)
* numpy (https://numpy.org/)

### Build standalone exe ###

* download pyinstaller (https://www.pyinstaller.org/)
* build .exe "pyinstaller --onefile --distpath bin --workpath pyinstaller_temp meres.py"
