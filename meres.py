import cv2 as cv
import numpy as np
import math
import sys
import os

def TimestampToTime(ts) : 
    hours = (ts // (1000 * 60 * 60))
    ts -= (hours * 1000 * 60 * 60)
    minutes = (ts // (1000 * 60)) 
    ts -= (minutes * 1000 * 60)
    seconds = (ts // 1000)
    ts -= (seconds * 1000)
    millisecs = ts

    return (hours, minutes, seconds, millisecs)

def FormatLaptime(startTs, endTs) :
    startTime = TimestampToTime(startTs)
    endTime = TimestampToTime(endTs)
    lapTime = endTs - startTs
    lapTimeSec = lapTime / 1000.0

    laptime = ""

    laptime += "{}:{:02}:{:02}.{:03}".format(startTime[0], startTime[1], startTime[2], startTime[3])
    laptime += " - "
    laptime += "{}:{:02}:{:02}.{:03}".format(endTime[0], endTime[1], endTime[2], endTime[3])
    laptime += " @ {:.3f}".format(lapTimeSec)

    return laptime

def PrintLaptimes(timestamps, fileName) : 
    file = None
    try : 
        file = open(fileName, "wt")
    except : 
        print("Cannot open file to write : " + fileName)
        file = None

    lapCount = (len(timestamps) - 1)
    if file : file.write("{}\n".format(lapCount))
    print("{}".format(lapCount))

    for i in range(0, lapCount, 1) : 
        startTs = timestamps[i]
        endTs = timestamps[i + 1]
        
        laptimeStr = FormatLaptime(startTs, endTs)

        if file : file.write("{}\n".format(laptimeStr))
        print("{}".format(laptimeStr))

    if file : file.close()

def Exit(error) : 
    print("Program terminated with error : " + error)
    input("Press Enter to exit...")
    sys.exit(0)

def HomographyOk(mat) : 
    PERSP_MAX_LEN = 0.002
    BASE_MAX_LEN = 4.0
    BASE_MIN_LEN = 0.1
    MIN_DET = 0.0

    result = False

    if mat is not None : 
        det = mat[0][0] * mat[1][1] - mat[1][0] * mat[0][1]
        perspX = mat[2][0]
        perspY = mat[2][1]
        perspLen = math.sqrt(perspX * perspX + perspY * perspY)
        baseXX = mat[0][0]
        baseXY = mat[1][0]
        baseXLen = math.sqrt(baseXX * baseXX + baseXY * baseXY)
        baseYX = mat[0][1]
        baseYY = mat[1][1]
        baseYLen = math.sqrt(baseYX * baseYX + baseYY * baseYY)
        print("Homography det : {:.8f}, perspLen : {:.8f}, baseXLen : {:.8f}, baseYLen {:.8f}".format(det, perspLen, baseXLen, baseYLen))

        result = (
            det > MIN_DET and
            perspLen >= 0.0 and perspLen < PERSP_MAX_LEN and
            baseXLen > BASE_MIN_LEN and baseXLen < BASE_MAX_LEN and
            baseYLen > BASE_MIN_LEN and baseYLen < BASE_MAX_LEN
        ) 

    return result

def MatchFLANN(matcher, refDescr, frameDescr) :
    matches = matcher.knnMatch(refDescr, frameDescr, k=2)
    # store all the good matches as per Lowe's ratio test.
    ratio = 0.75
    good = []
    for m,n in matches:
        if m.distance < ratio*n.distance:
            good.append(m)

    return good

def CheckArguments() : 
    if len(sys.argv) < 2 : 
        Exit("Argument error - no video file specified.")
    else : 
        print("Using arguments : ")
        for arg in sys.argv : 
            print(arg)

    return sys.argv[1]

def CheckWorkFolder() : 
    folder = None
    exeFolder, exeName = os.path.split(sys.executable)
    userFolder = os.path.expanduser("~")
    if exeName != "python.exe" and os.access(exeFolder, os.W_OK) : 
        folder = exeFolder
    elif os.access(userFolder, os.W_OK) :  
        folder = userFolder
    else : 
        Exit("Folder error - cannot write to folder {} , {}".format(exeFolder, userFolder))
    
    print("Using work folder : " + folder);
    return folder

def OpenVideo(fileName) : 
    video = cv.VideoCapture(fileName)
    if video is None or video.isOpened() == False : 
        Exit("Video error - cannot open file : " + fileName)
    else : 
        print("Video opened : " + fileName)

    return video

def OpenImage(fileName) : 
    #NOTE : Unicode bug workaround
    image = cv.imdecode(np.fromfile(fileName, dtype=np.uint8), cv.IMREAD_UNCHANGED)
    if image is None : 
        Exit("Image error - cannot open file : " + fileName)
    else : 
        print("Image opened : " + fileName)

    return image


def OpenStartImage() : 
    START_IMAGE = "start.png"
    folders = [os.path.split(sys.executable)[0], os.path.split(sys.argv[0])[0]]
    image = None

    for folder in folders : 
        path = os.path.join(folder, START_IMAGE)
        if os.path.exists(path) :
            print("Loading image with path : " + path)
            image = OpenImage(path)
            break
        else : 
            print("Image not found on path : " + path)

    if image is None : 
        Exit("Image error - cannot find image : " + START_IMAGE)

    return image

def WriteImage(image, path) : 
    status, buffer = cv.imencode(".jpg", image)
    if status is True :
        buffer.tofile(path)
        print("Image written to : " + path)
    else : 
        print("Could not write image to : " + path)

def InitSift() : 
    sift = cv.SIFT_create(5000, 8)
    FLANN_INDEX_KDTREE = 4
    index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
    search_params = dict(checks = 50)
    flann = cv.FlannBasedMatcher(index_params, search_params)

    return (sift, flann)

def Main() : 
    FRAME_SKIP = 16
    START_SEC = 0
    RESIZE_WIDTH = 1024
    RESIZE_HEIGHT = 576
    MIN_MATCH_COUNT = 8
    TRACKING_TTL = 30
    GENERATE_PROOF_IMAGES = True

    print("KIBASZOTT MERES")

    workFolder = CheckWorkFolder()

    videoFile = CheckArguments()
    video = OpenVideo(videoFile)
    startImage = OpenStartImage()

    frameCount = int(video.get(cv.CAP_PROP_FRAME_COUNT))
    video.set(cv.CAP_PROP_POS_MSEC, (START_SEC * 1000))

    sift, flann = InitSift()
    refKp, refDescr = sift.detectAndCompute(startImage, None);

    frameTsList = []
    frameIDList = []
    
    tracking = False
    trackingLostTs = 0
    trackingLostFrameID = 0
    trackingTTL = 0
    
    hasFrame = True
    while hasFrame :  
        frameID = int(video.get(cv.CAP_PROP_POS_FRAMES))
        frameTs = int(video.get(cv.CAP_PROP_POS_MSEC))
        print("FrameID : {}/{} FrameTs : {}".format(frameID, frameCount, frameTs))

        hasFrame, frame = video.read()
        processFrame = hasFrame and ((frameID % FRAME_SKIP) == 0) or (tracking)

        if processFrame : 
            print("Processing...")
            frame = cv.cvtColor(frame, cv.COLOR_BGR2GRAY)
            frame = cv.resize(frame, (RESIZE_WIDTH, RESIZE_HEIGHT))

            frameKp, frameDescr = sift.detectAndCompute(frame, None);
            matches = MatchFLANN(flann, refDescr, frameDescr)

            if len(matches) > MIN_MATCH_COUNT:
                refPts = np.float32([ refKp[m.queryIdx].pt for m in matches ]).reshape(-1,1,2)
                framePts = np.float32([ frameKp[m.trainIdx].pt for m in matches ]).reshape(-1,1,2)
                H, mask = cv.findHomography(refPts, framePts, cv.RANSAC, 5.0)
                if HomographyOk(H) : 
                    print("Tracking target (Homograpy ok).")
                    tracking = True
                    trackingLostTs = frameTs
                    trackingLostFrameID = frameID
                    trackingTTL = TRACKING_TTL
                else : 
                    print("Target lost (Homography rejected). Lastseen : " + str(frameTs) + " ttl : " + str(trackingTTL))
            else : 
                print("Target lost (Not enough matches). Lastseen : " + str(frameTs) + " ttl : " + str(trackingTTL))

            if tracking :
                trackingTTL -= 1
                if trackingTTL < 0 : 
                    frameTsList.append(trackingLostTs)
                    frameIDList.append(trackingLostFrameID)
                    tracking = False
        else : 
            print("Skipping...")

    baseFilename = os.path.splitext(os.path.split(videoFile)[1])[0]
    workFolder = os.path.join(workFolder, "meres_" + baseFilename)
    os.makedirs(workFolder, exist_ok=True)
    print("Results will be written to : " + workFolder)

    if GENERATE_PROOF_IMAGES : 
        lapCount = (len(frameIDList) - 1)
        for i in range(0, lapCount, 1) :
            video.set(cv.CAP_PROP_POS_FRAMES, frameIDList[i])
            hasFrame0, frame0 = video.read()
            video.set(cv.CAP_PROP_POS_FRAMES, frameIDList[i + 1])
            hasFrame1, frame1 = video.read()
            if hasFrame0 and hasFrame1 : 
                sideBySide = np.concatenate((frame0, frame1), axis = 1)
                path = os.path.join(workFolder, "Lap_{}".format(i) + ".jpg")
                #NOTE : Unicode bug workaround
                #cv.imwrite(path, sideBySide)
                WriteImage(sideBySide, path)

    path = os.path.join(workFolder, "laptimes.txt")
    PrintLaptimes(frameTsList, path)

if __name__ == "__main__" :
    try : 
        Main()
    except Exception as e : 
        print(e)

    input("Press Enter to exit...")



"""


            #height, width = start.shape
            #homographyRect = np.float32([ [0,0],[0,height-1],[width-1,height-1],[width-1,0] ]).reshape(-1,1,2)
            #homographyRect = cv.perspectiveTransform(homographyRect, H)
            #frame = cv.polylines(frame, [np.int32(homographyRect)],True,color,3, cv.LINE_AA)

            #debugImage = cv.drawMatches(start, rKp, frame, fKp, matches, None)
            #debugImage = None
            #debugImage = cv.drawMatches(start, refKp, frame, frameKp, matches[:50], debugImage, flags=2)
            #cv.imshow("DebugImage", debugImage)
            #Dont know why but this is needed for the window????
            #if cv.waitKey(1) == ord('n') : 
            #	pass



                if M is not None : 
                    color = (255, 255, 255)
                    matchesMask = mask.ravel().tolist()
                    h,w = start.shape
                    d = 0
                    pts = np.float32([ [0,0],[0,h-1],[w-1,h-1],[w-1,0] ]).reshape(-1,1,2)
                    dst = cv.perspectiveTransform(pts,M)
                    det22 = M[0][0] * M[1][1] - M[1][0] * M[0][1]
                    perspX = M[2][0]
                    perspY = M[2][1]
                    perspLen = math.sqrt(perspX * perspX + perspY * perspY)
                    if det22 > 0 : 
                        if perspLen < PERSP_LIMIT :
                            #print(M)
                            tWidth = dst[3].max() - dst[0].max()
                            tHeight = dst[1].min() - dst[0].min()
                            tAr = tWidth / tHeight
                            rAr = w / h
                            print("Ts : " + str(video.get(cv.CAP_PROP_POS_MSEC)))
                            print("Size orig : {:.4f} x {:.4f}, transformed : {:.4f} x {:.4f}".format(w, h, tWidth, tHeight))
                            print("Ar orig : {:.4f}, transformed : {:.4f}".format(rAr, tAr))
                            print("Det22 : " + str(det22))
                            print("Scale : [" + str(M[0][0]) + ", " + str(M[1][1]) + ", " + str(M[2][2]) + " ]")
                            print("Perspective : [ {:.8f} , {:.8f} ], {:.8f}".format(perspX, perspY, perspLen))
                        else : 
                            color = (255, 0, 0)
                            print("Perspective rejected.")
                    else : 
                        print("Negative det rejected.")
                        color = (255, 0, 0)

                    img2 = cv.polylines(frame, [np.int32(dst)],True,color,3, cv.LINE_AA)

                else : 
                    print("Cannot create homography.")
            else:
                print( "Not enough matches are found - {}/{}".format(len(good), MIN_MATCH_COUNT) )
                matchesMask = None

            draw_params = dict(matchColor = (0,255,0), # draw matches in green color
                   singlePointColor = None,
                   matchesMask = matchesMask, # draw only inliers
                   flags = 2)
            img3 = cv.drawMatches(start,rKp,frame,fKp,good,None,**draw_params)
            cv.imshow("DebugImage", img3)
            #plt.imshow(img3, 'gray'),plt.show()



            #dist = [m.distance for m in matches]
            #thres_dist = (sum(dist) / len(dist)) * 0.5
            #matches = [m for m in matches if m.distance < thres_dist]    

            # Draw first 50 matches.
            #debugImage = None
            #debugImage = cv.drawMatches(start, rKp, frame, fKp, matches[:50], debugImage, flags=2)

            #print("Match count : " + str(len(matches)))
            #cv.imshow("DebugImage", debugImage)

            #Wait for n key
            if cv.waitKey(0) == ord('n') : 
                pass
            
    


"""
